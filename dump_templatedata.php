<?php

if ( count( $argv ) !== 4 ) {
	print "Usage:\n\tphp {$argv[0]} mysql:dbname=enwiki username password\n";
	exit(-1);
}

list($db, $username, $password) = array_slice( $argv, 1 );

$pdo = new PDO($db, $username, $password);
$statement = $pdo->prepare('
	select pp_page, pp_value
	from page_props
	where
		pp_propname = \'templatedata\'
');

$res = $statement->execute();

while ( $row = $statement->fetch() ) {
	$decompressed = gzdecode($row['pp_value']);
    if ( !$decompressed ) {
        fwrite(STDERR, "Bad templatedata blob for page {$row['pp_page']}\n");
    } else {
        $data = json_decode($decompressed, true);
        $data['_pageid'] = $row['pp_page'];
        $decompressed = json_encode($data);
        print($decompressed . "\n");
    }
}
